# Space Inc

> Handong Capstone Design Project

### 약속

1. 주 40시간 (월~일)
   * 못 채우는 경우, 벌금 1만원
   * 이후에 늘릴 예정
   * 정섭은 17주차에 한 시간을 18주와 19주에 이월 가능
   * 출퇴근은 git으로  commit
   * 위 약속은 23일 부터 시행
2. code review
   * 1월 1일 부터  4시에 랩실 304호에서 1시간 반 동안 code review 및 tea time
   * 주중 (월 부터 금 합의한 날을 제외한 모든 날) 실행
3. 임시 1월 27일  면담 후, 근무 시간 조정

---



### 진행

| 주차  | Brian                                      | Kyu                                        |
| ----- | ------------------------------------------ | ------------------------------------------ |
| 1주차 | pre_튜토리얼 1                             | 시험                                       |
| 2주차 | pre_튜토리얼 2                             | pre_큰 그림                                |
| 3주차 | pre_창작                                   | Pre-큰 그림(cont)                          |
| 4주차 | Space_Inc( :exclamation: to be discussed ) | Space_Inc( :exclamation: to be discussed ) |
| 5주차 | Sprint                                     | Sprint                                     |
|       |                                            |                                            |
|       |                                            |                                            |
|       |                                            |                                            |
|       |                                            |                                            |



## 출퇴

### 12월

| 1주차 | 월   | 화   | 수   | 목   | 금   | 토 |일 |
| ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| Brain | :white_check_mark::white_check_mark:(2시간) |      |      |      |:white_check_mark:(출근,9:24):white_check_mark:(3시간)      |::white_check_mark:(출근,11:14)||
| Kyu | :white_check_mark::white_check_mark: ​(2시간) |      |      |      |      |::white_check_mark:(퇴근,17:19)<br />(5시간)||


| 2주차 | 월   | 화   | 수   | 목   | 금   | 토 |일 |
| ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| Brain |  |      |      |      |      |||
| Kyu |  | :white_check_mark:(1시8분):white_check_mark:(2시51분)<br />(1시간 43분) |  |      |      |||

