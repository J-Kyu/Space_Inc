# Git Lab

> 목표: Gitlab과 GitHub의 Space_Inc를 연동하여, CI/CD 개발 환경을 구축한다.

## 연동 방법

1. GitLab 계정을 만든다
2. Github 계정과 연동한다
3. 연동된  Github account의  repository를  import한다

## [Auto DevOps](<https://gitlab.com/help/topics/autodevops/index.md>)

> Gitlab에서는 auto devops 를 통해 자동 빌드,  배포, 모니터링이 가능하다.

## Gitlab Issue Tracking

```
Ref J-Kyu/Space_Inc#(issue reference)
(explanation)
```

## Gitlab SSH

* [ssh](<https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair>)
* 